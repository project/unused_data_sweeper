# Unused Data Sweeper

The "Unused Data Sweeper" module is a Drupal 9||10 custom module tailored to
address data management concerns. It enables administrators to efficiently
identify, monitor, and manage unused or obsolete data, thereby optimizing the
website's performance and maintaining its content's relevancy.

### Features

- **Filtering & Search:** Provides forms that allow filtering users, Taxonomy
  and content based on various parameters like content type, creation date,
  status, user role, termid, vocubulary etc.

- **Data Management Tools:** Lists Users, and unpublished nodes & Terms allows
  administrators to perform delete and edit operations.

- **Integration with Drupal Taxonomy:** Ability to interact with taxonomy terms,
  fetch vocabulary details, and manage them.

- **Theming and Styling:** Contains CSS to ensure consistency in the admin theme
  irrespective of the site's active theme.

- **Safety Measures:** Includes checks and error handlers for potential database
  issues or errors.

- **Extensibility:** Built with the future in mind, ensuring it's extensible to
  accommodate more features or integrations.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/unused_data_sweeper)

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/unused_data_sweeper)


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

The module requires a permission, you should have a administrator permissions.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

After Installation User can simply visit this module from Help Section, Or By
Clicking Config button.

The main aim to create this module is if you have lots of unused Node,
term on current site. So with the help of this module you can simply list all
unused data.


## Maintainers

- Bhupendra Raykhere - [Bhupendra_Raykhere](https://www.drupal.org/u/bhupendra_raykhere)
- Priyansh Chourasiya - [Priyansh Chourasiya](https://www.drupal.org/u/priyansh-chourasiya)
- Vijay Sharma - [VijaySharma_89](https://www.drupal.org/u/vijaysharma_89)
