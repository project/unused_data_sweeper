(function ($) {
  Drupal.behaviors.autoRemoveStatusMessages = {
    attach: function (context, settings) {
      setTimeout(function () {
        $('.messages').fadeOut('slow');
      }, 10000 ); // This waits for 5 seconds (5000 milliseconds)
    }
  };
})(jQuery);
