<?php
namespace Drupal\unused_data_sweeper\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Entity\User;
use Drupal\Component\Render\FormattableMarkup;
use Drupal\views\Entity\View;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Database\Connection;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class TaxonomyDeleteForm extends FormBase {
/**   
   * {@inheritdoc}
   */
  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;
  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;
  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;
  /**
    * The Messenger service.
    *
    * @var \Drupal\Core\Messenger\MessengerInterface
    */
    protected $messenger;
  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;
  /**
 * The entity field manager.
 *
 * @var \Drupal\Core\Entity\EntityFieldManagerInterface
 */
  protected $entityFieldManage;
   /**
 * The entity field manager.
 *
 * @var \Drupal\Core\Extension\ModuleHandlerInterface
 */
protected $moduleHandler;
   /**
   * Constructs a new UserRoleReportController object.
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The Drupal service container.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack used to retrieve the current request.
   *  @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service
   *  @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   *  @param \Drupal\Core\Entity\EntityFieldManagerInterface $entityFieldManage
   *   The Entity FieldManager Interface.
   *  @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The Module Handler Interface.
   */
  
  public function __construct(EntityTypeManagerInterface $entity_type_manager, Connection $connection,  RequestStack $request_stack,MessengerInterface $messenger,DateFormatterInterface $date_formatter,EntityFieldManagerInterface $entityFieldManage
  ,ModuleHandlerInterface $module_handler
  ){
    $this->entityTypeManager = $entity_type_manager;
    $this->connection = $connection;
    $this->requestStack = $request_stack;
    $this->messenger = $messenger;
    $this->dateFormatter = $date_formatter;
    $this->entityFieldManage = $entityFieldManage;
    $this->moduleHandler = $module_handler;
  }
  public static function create(ContainerInterface $container){

    return new static(
      $container->get('entity_type.manager'),
      $container->get('database'),
      $container->get('request_stack'),
      $container->get('messenger'),
      $container->get('date.formatter'),
      $container->get('entity_field.manager'),
      $container->get('module_handler')
    );

  }
  public function getFormId() {
      return 'taxonomy_delete_form';
  }

  public function buildForm( array $form, FormStateInterface $form_state ) {
    $form[ 'table' ] = [
        '#type' => 'tableselect',
        '#header' => $this->getHeader(),
        '#options' => $this->getTableRows() ?: [],
        '#empty' => $this->t( 'No unused terms found.' ),
    ];

    $form[ 'submit' ] = [
        '#type' => 'submit',
        '#value' => $this->t( 'Delete Selected Terms' ),
        '#attributes' => [
          'class' => ['btn btn-danger'],
        ],
    ];
    $form[ '#attached' ][ 'library' ][] = 'unused_data_sweeper/my_styles';
    $form['pager']=[
      '#type' => 'pager',
    ];

    return $form;
  }

  protected function getHeader() {
    return [
        'term_id' => $this->t( 'Term ID' ),
        'term_name' => $this->t( 'Term Name' ),
        'vocab_name' => $this->t('Vocabulary'),
        'status' => $this->t('Status'),
        'modified_date' => $this->t( 'Last Modified Date' ),
        'operations' => $this->t( 'Operations' ),
    ];
  }

  protected function getTableRows() {
    $request = $this->requestStack->getCurrentRequest();
    // The code from controller that fetches and prepares term data for the table rows.
    // Load all terms.
    $all_terms = $this->entityTypeManager->getStorage( 'taxonomy_term' )->loadMultiple();

    /* Node Section Start */
      // Check taxonomy references in nodes.
      $node_tids = [];

      $node_types = $this->entityTypeManager->getStorage( 'node_type' )->loadMultiple();
      foreach ( $node_types as $node_type ) {
          $fields = $this->entityFieldManage->getFieldDefinitions( 'node', $node_type->id() );
          foreach ( $fields as $field_name => $field_definition ) {
              if ( $field_definition->getType() == 'entity_reference' && $field_definition->getSetting( 'target_type' ) == 'taxonomy_term' ) {
                  $table_name = 'node__' . $field_name;
                  if ( $this->connection->schema()->tableExists( $table_name ) ) {
                      $field_column = $field_name . '_target_id';
                      $result = $this->connection->select( $table_name, 't' )
                      ->fields( 't', [ $field_column ] )
                      ->distinct()
                      ->execute()
                      ->fetchCol();
                      $node_tids = array_merge( $node_tids, $result );
                      // dd( $node_tids );
                  }
              }
          }
      }
    /* Node Section End */

    /*Views Section Start */
      $all_terms_in_views = [];
      // Load all views.
      $views = View::loadMultiple();
      foreach ( $views as $view ) {

          foreach ( $view->getDisplay( 'default' )[ 'display_options' ][ 'filters' ] as $filter ) {

              // Check if the filter is for taxonomy term.
              if ( isset( $filter[ 'field' ] ) && $filter[ 'field' ] == 'tid' && $filter[ 'plugin_id' ] == 'taxonomy_index_tid' ) {

                  // Check if specific terms are set in the filter.
                  if ( isset( $filter[ 'value' ] ) && is_array( $filter[ 'value' ] ) ) {
                      foreach ( $filter[ 'value' ] as $term_id ) {
                          $all_terms_in_views[ $term_id ] = $term_id;
                      }

                  }
              }
          }
      }
      // Load the taxonomy terms based on the collected IDs.
      $used_terms_in_views = $this->entityTypeManager->getStorage( 'taxonomy_term' )->loadMultiple( $all_terms_in_views );
      //convert it to array keys
      $used_term_view_ids = array_keys( $used_terms_in_views );
    /*Views Section End */

   /*Block Section Start */
    // Check taxonomy references in blocks ( assuming block content ).
    $referenced_block_tids = [];
    $block_types = $this->entityTypeManager->getStorage( 'block_content_type' )->loadMultiple();
    foreach ( $block_types as $block_type ) {
        $block_fields = $this->entityFieldManage->getFieldDefinitions( 'block_content', $block_type->id() );
        foreach ( $block_fields as $field_name => $field_definition ) {
            if ( $field_definition->getType() == 'entity_reference' && $field_definition->getSetting( 'target_type' ) == 'taxonomy_term' ) {
                $table_name = 'block_content__' . $field_name;
                if ( $this->connection->schema()->tableExists( $table_name ) ) {
                    $field_column = $field_name . '_target_id';
                    $result = $this->connection->select( $table_name, 't' )
                    ->fields( 't', [ $field_column ] )
                    ->distinct()
                    ->execute()
                    ->fetchCol();
                    $referenced_block_tids = array_merge( $referenced_block_tids, $result );
                }

            }
        }
    }
   /*Block Section End */

   /*Paragraph Section Start*/
    // Check taxonomy references in parargraph.
    $referenced_paragraph_tids = [];

    // Only proceed if the Paragraphs module is installed.
    if ( $this->moduleHandler->moduleExists( 'paragraphs' ) ) {
      // Get all paragraph types.
      $paragraph_types = $this->entityTypeManager->getStorage( 'paragraphs_type' )->loadMultiple();
      // For each paragraph type, check each field.
      foreach ( $paragraph_types as $paragraph_type ) {
        $fields = $this->entityFieldManage->getFieldDefinitions( 'paragraph', $paragraph_type->id() );
        foreach ( $fields as $field_name => $field_definition ) {
          if ( $field_definition->getType() == 'entity_reference' && $field_definition->getSetting( 'target_type' ) == 'taxonomy_term' ) {
            // Query to find all terms used by this field in this paragraph type.
            $table_name = 'paragraph__' . $field_name;
            if ( $this->connection->schema()->tableExists( $table_name ) ) {
              $field_column = $field_name . '_target_id';
              $result = $this->connection->select( $table_name, 't' )
              ->fields( 't', [ $field_column ] )
              ->distinct()
              ->execute()
              ->fetchCol();
              // Merge the result to the list of used term IDs.
              $referenced_paragraph_tids = array_merge( $referenced_paragraph_tids, $result );
            }
          }
        }
      }
    }
   /*Paragraph Section End*/

   /*User List Starts */
    // Load all user entities.
    $users = User::loadMultiple();
    $referenced_user_tids = [];
    foreach ( $users as $user ) {
      // Get all fields for the user.
      foreach ( $user->getFieldDefinitions() as $field_name => $field_definition ) {
        // Check if the field is an entity reference to taxonomy terms.
        if ( $field_definition->getType() == 'entity_reference' && $field_definition->getSetting( 'target_type' ) == 'taxonomy_term' ) {
          // Get the term ids referenced by this field for this user.
          $term_ids = array_column( $user->get( $field_name )->getValue(), 'target_id' );
          $referenced_user_tids = array_merge( $referenced_user_tids, $term_ids );
        }
      }
    }
   /*User List Ends */

   // Here we will merger all the array which have taxonomy terms.
    $merged_array = array_merge( $node_tids, $used_term_view_ids, $referenced_block_tids, $referenced_user_tids, $referenced_paragraph_tids );
    $unique_merged_array = array_unique( $merged_array );
    $unused_tids = array_diff( array_keys( $all_terms ), $unique_merged_array );

    // Will get a parameter from query;
    $term_id = $request->query->get( 'termid' );
    $vocabulury =  $request->query->get( 'vocabulary' );
    $term_status = $request->query->get('status');

    //If Want to Filter on the basis of Vocabulary
    if ((!empty($vocabulury) || $term_status !='select') && $term_id) {
      $this->messenger()->addError( $this->t( 'You Can only Filter with id' ) );
    }
    elseif ( isset($vocabulury) && !empty($vocabulury) && empty($term_id) ){
      $query = $this->entityTypeManager->getStorage('taxonomy_term')->getQuery();
      $query->condition( 'vid', $vocabulury )
            ->condition('tid', $unused_tids, 'IN')
            ->accessCheck(FALSE)
            ->pager(5);
      //if want to filter on vocabulary with status
      if($term_status == 0 || $term_status == 1){ 
          $query->condition('status',$term_status); 
      }     
      $tids = $query->execute();
      // $commonElements = array_intersect($tids,$unused_tids);
      $vocab_terms = $this->entityTypeManager->getStorage( 'taxonomy_term' )->loadMultiple($tids);
      
      $rows = [];
      foreach ( $vocab_terms as $term ) {
        $edit_url = Url::fromRoute( 'entity.taxonomy_term.edit_form', [ 'taxonomy_term' => $term->id() ] );
        $edit_link = Link::fromTextAndUrl( t( 'Edit' ), $edit_url )->toString();
        $delete_url = Url::fromRoute( 'entity.taxonomy_term.delete_form', [ 'taxonomy_term' => $term->id() ] );
        $delete_link = Link::fromTextAndUrl( t( 'Delete' ), $delete_url )->toString();
        // Render the links.
        $links = new FormattableMarkup( '@edit_link | @delete_link', [ '@edit_link' => $edit_link, '@delete_link' => $delete_link ] );
        $modified_timestamp = $term->get( 'changed' )->value;
        $modified_date = $this->dateFormatter->format( $modified_timestamp, 'custom', 'Y-m-d H:i:s' );
        $rows[ $term->id() ] = [
          'term_id' => $term->id(),
          'term_name' => $term->getName(),
          'vocab_name' => $term->bundle(),
          'status' => $term->isPublished() ? $this->t('Published') : $this->t('Unpublished'),
          'modified_date' => $modified_date,
          'operations' => $links,
        ];
      }
      return $rows;
    }elseif (in_array($term_id, $unused_tids)) {
      if (isset($term_id) && !empty($term_id)) {
        $term = $this->entityTypeManager->getStorage( 'taxonomy_term' )->load( $term_id );
        $rows = [];
        $edit_url = Url::fromRoute( 'entity.taxonomy_term.edit_form', [ 'taxonomy_term' => $term->id() ] );
        $edit_link = Link::fromTextAndUrl( t( 'Edit' ), $edit_url )->toString();
        $delete_url = Url::fromRoute( 'entity.taxonomy_term.delete_form', [ 'taxonomy_term' => $term->id() ] );
        $delete_link = Link::fromTextAndUrl( t( 'Delete' ), $delete_url )->toString();
        // Render the links.
        $links = new FormattableMarkup( '@edit_link | @delete_link', [ '@edit_link' => $edit_link, '@delete_link' => $delete_link ] );
        $modified_timestamp = $term->get( 'changed' )->value;
        $modified_date = $this->dateFormatter->format( $modified_timestamp, 'custom', 'Y-m-d H:i:s' );

        $rows[ $term->id() ] = [
          'term_id' => $term->id(),
          'term_name' => $term->getName(),
          'vocab_name' => $term->bundle(),
          'status' => $term->isPublished() ? $this->t('Published') : $this->t('Unpublished'),
          'modified_date' => $modified_date,
          'operations' => $links,
        ];
        return $rows;
      }
    }
    //By Default fetch all the unused terms
    else{
        if( isset( $term_id ) && !empty( $term_id )) {
          $this->messenger()->addError( $this->t( 'Selected TermID Not Found In Unused Terms' ) );
        }
        $query = $this->entityTypeManager->getStorage('taxonomy_term')->getQuery()
          ->condition('tid', $unused_tids, 'IN')
          ->pager(5)
          ->accessCheck(FALSE);
        if(!empty($term_status)){        
          if($term_status == 0 || $term_status == 1){ 
            $query->condition('status',$term_status); 
          }
        }
        $unused_tid = $query->execute();
        $unused_terms = $this->entityTypeManager->getStorage('taxonomy_term')->loadMultiple($unused_tid);
        //Prepare a table for rendering.
        $rows = [];
        foreach ( $unused_terms as $term ) {
        $edit_url = Url::fromRoute( 'entity.taxonomy_term.edit_form', [ 'taxonomy_term' => $term->id() ] );
        $edit_link = Link::fromTextAndUrl( t( 'Edit' ), $edit_url )->toString();
        $delete_url = Url::fromRoute( 'entity.taxonomy_term.delete_form', [ 'taxonomy_term' => $term->id() ] );
        $delete_link = Link::fromTextAndUrl( t( 'Delete' ), $delete_url )->toString();
        // Render the links.
        $links = new FormattableMarkup( '@edit_link | @delete_link', [ '@edit_link' => $edit_link, '@delete_link' => $delete_link ] );
        $modified_timestamp = $term->get( 'changed' )->value;
        $modified_date = $this->dateFormatter->format( $modified_timestamp, 'custom', 'Y-m-d H:i:s' );

        $rows[ $term->id() ] = [  
          'term_id' => $term->id(),
          'term_name' => $term->getName(),
          'vocab_name' => $term->bundle(),
          'status' => $term->isPublished() ? $this->t('Published') : $this->t('Unpublished'),
          'modified_date' => $modified_date,
          'operations' => $links,
        ];
      }
      return $rows;
    }
  }

  public function submitForm( array &$form, FormStateInterface $form_state ) {
    $selected_terms = array_filter( $form_state->getValue('table') );
    if ( !empty( $selected_terms ) ) {
      // Delete each selected term.
      $term_storage = $this->entityTypeManager->getStorage( 'taxonomy_term' );
      $entities = $term_storage->loadMultiple( $selected_terms );
      $term_storage->delete( $entities );
      $this->messenger()->addMessage( $this->t( 'Selected terms have been deleted.' ) );
      if ( !$term_storage ) {
        $this->messenger()->addError( $this->t( 'No Term Selected' ) );
      }
    } else {
      $this->messenger()->addWarning( $this->t( 'No Term Selected' ) );
    }
  }
}