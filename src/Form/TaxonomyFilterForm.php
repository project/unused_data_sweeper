<?php

namespace Drupal\unused_data_sweeper\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Url;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 *
 */
class TaxonomyFilterForm extends FormBase {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new TaxonomyFilterForm.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   *
   */
  public function getFormId() {
    return 'taxonomy_filter_form';
  }

  /**
   *
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['title'] = [
      '#type' => 'markup',
      '#markup' => $this->t('<b>All The Unused( Not used in any Node, Paragraph, Views, User, Block ) taxonomy term is listed here.</b>'),
    ];
    $form['term_id'] = [
      '#type' => 'number',
      '#title' => $this->t('Term ID'),
      '#description' => $this->t('Filter by term_id'),
      '#required' => FALSE,
    ];

    $vocabularies = $this->entityTypeManager->getStorage('taxonomy_vocabulary')->loadMultiple();
    $options = ['-Select-'];
    foreach ($vocabularies as $vocabulary) {
      $options[$vocabulary->id()] = $vocabulary->label();
    }
    $form['vocabulary'] = [
      '#type' => 'select',
      '#title' => $this->t('Vocabulary'),
      '#description' => $this->t('Filter by Vocabulary'),
      '#options' => $options,
      '#required' => FALSE,
    ];

    $form['status'] = [
      '#type' => 'select',
      '#title' => $this->t('Status'),
      '#options' => [
        'select' => $this->t('-Select-'),
        '1' => $this->t('Published'),
        '0' => $this->t('Unpublished'),
      ],
      '#required' => FALSE,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Filter'),
      '#attributes' => [
        'class' => ['btn btn-success'],
      ],
    ];
    $form['actions']['reset'] = [
      '#type' => 'submit',
      '#value' => $this->t('Reset'),
      '#submit' => ['::resetForm'],
      '#limit_validation_errors' => [],
      '#attributes' => [
        'class' => ['btn btn-warning'],
      ],
    ];
    $form['actions']['redirect'] = [
      '#type' => 'submit',
      '#value' => $this->t('Go Back'),
      '#limit_validation_errors' => [],
      '#submit' => ['::redirectForm'],
      '#attributes' => [
        'class' => ['btn btn-danger'],
      ],
    ];
    $form['#validate'][] = '::my_form_validate';
    return $form;
  }

  /**
   * Form validation handler.
   */
  public function my_form_validate(array $form, FormStateInterface $form_state) {
    $vocab = $form_state->getValue('vocabulary');
    $t_id = $form_state->getValue('term_id');
    $status = $form_state->getValue('status');
    // dd($status);
    // Ensure that one of any field is selected.
    if (empty($vocab) && empty($t_id) && $status == 'select') {
      $form_state->setErrorByName('term_id', t('Please Select atleast 1 field.'));
    }
  }

  /**
   *
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $vocab = $form_state->getValue('vocabulary');
    $t_id = $form_state->getValue('term_id');
    $status = $form_state->getValue('status');
    $url = Url::fromRoute('unused_data_sweeper.taxonomy_list')->setRouteParameters(['termid' => $t_id, 'vocabulary' => $vocab, 'status' => $status]);
    $form_state->setRedirectUrl($url);
  }

  /**
   *
   */
  public function resetForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRedirect('unused_data_sweeper.taxonomy_list');
  }

  /**
   *
   */
  public function redirectForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRedirect('unused_data_sweeper.form');
  }

}
