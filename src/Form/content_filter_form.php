<?php

namespace Drupal\unused_data_sweeper\Form;

use Drupal\Core\Url;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 *
 */
class Content_Filter_Form extends FormBase {

  /**
   * {@inheritdoc}
   */

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new UserRoleReportController object.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The Drupal service container.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   *
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
    );
  }

  /**
   *
   */
  public function getFormId() {
    return 'unused_data_sweeper_content_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['message'] = [
      '#type' => 'markup',
      '#markup' => $this->t('<b style="font-size: 20px">On this page All the Unpublished node will listed & Can perform dalete and edit operation as well Also filter on the basis of content type and created date.</b>'),
    ];

    // Fetch all content types.
    $content_types = $this->entityTypeManager->getStorage('node_type')->loadMultiple();
    $content_type_options = ['-select-'];

    foreach ($content_types as $content_type) {
      $content_type_options[$content_type->id()] = $content_type->label();
    }

    $form['content_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Content Type'),
      '#options' => $content_type_options,
      '#attributes' => [
        'class' => ['form-control'],
      ],
    ];
    $form['details'] = [
      '#type' => 'details',
      '#title' => t('Search User From Created Date To Choice Date'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#open' => TRUE,
    ];
    $form['details']['start_date'] = [
      '#type' => 'date',
      '#title' => $this->t('Start Date'),
      '#description' => "Select Date",
      '#attributes' => [
        'class' => ['user-start-date-field'],
      ],
      '#max' => date('Y-m-d') ,
    ];
    $form['details']['end_date'] = [
      '#type' => 'date',
      '#title' => $this->t('End Date'),
      '#description' => "Select Date",
      '#attributes' => [
        'class' => ['user-end-date-field'],
      ],
      '#max' => date('Y-m-d') ,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
      '#attributes' => [
        'class' => ['btn btn-success'],
      ],
    ];

    $form['actions']['reset'] = [
      '#type' => 'submit',
      '#value' => $this->t('Reset'),
      '#submit' => ['::resetForm'],
      '#limit_validation_errors' => [],
      '#attributes' => [
        'class' => ['btn btn-danger'],
      ],
    ];
    $form['actions']['redirect'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Go Back'),
      '#submit' => array('::redirectForm'),
      '#attributes' => [
        'class' => ['btn btn-danger'],
      ],
    );
    $form['#attached']['library'][] = 'unused_data_sweeper/my_styles';
    $form['#validate'][] = '::my_form_validate';

    return $form;
  }
  /**
   * Form validation handler.
   */
  function my_form_validate(array $form, FormStateInterface $form_state) {
    $start_date = $form_state->getValue('start_date');
    $end_date = $form_state->getValue('end_date');
    if(!empty($start_date) && empty($end_date)){
      $end_date = date('Y-m-d');
    }
    // Ensure end date is after or equal to start date
    if ($start_date && $end_date && ($end_date) <   ($start_date)) {
        $form_state->setErrorByName('end_date', t('End date should be After or the same as the start date.'));
    }
  }
  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $start_date = $form_state->getValue('start_date');
    $end_date = $form_state->getValue('end_date');
    if(!empty($start_date) && empty($end_date)){
      $end_date = date('Y-m-d');
    }
    $content_type = $form_state->getValue('content_type');
    // $start_date = $form_state->getValue('start_date');
    // $end_date = $form_state->getValue('end_date');
    $url = Url::fromRoute('unused_data_sweeper.content_list')->setRouteParameters(['cont_type' => $content_type, 'start_date' => $start_date, 'end_date' => $end_date]);
    $form_state->setRedirectUrl($url);
  }
  /**
   * {@inheritdoc}
   */
  public function resetForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRedirect('unused_data_sweeper.content_list'); 
  }
  public function redirectForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRedirect('unused_data_sweeper.form'); 
  }
}
