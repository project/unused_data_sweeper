<?php

namespace Drupal\unused_data_sweeper\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 *
 */
class unusedFilter extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'unused_data_sweeper_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['desc'] = [
      '#type' => 'markup',
      '#markup' => $this->t('<b>Unused Data Sweeper is used for listing unused data which is present on site. User can visit all the content By clicking on button. Four buttons are listed here for Content, Taxonomy, Users respectively: </b>'),
    ];
    $form['content_button'] = [
      '#type' => 'submit',
      '#value' => $this->t('Content'),
      '#submit' => ['::redirectToContent'],
    ];

    $form['taxonomy_button'] = [
      '#type' => 'submit',
      '#value' => $this->t('Taxonomy'),
      '#submit' => ['::redirectToTaxonomy'],
    ];

    $form['user_button'] = [
      '#type' => 'submit',
      '#value' => $this->t('User'),
      '#submit' => ['::redirectToUser'],
    ];

    $form['#attached']['library'][] = 'unused_data_sweeper/my_styles';

    return $form;
  }

  /**
   *
   */
  public function custom_forms_datetime_radio_callback(array &$form, FormStateInterface $form_state) {
    return $form['schedule_options']['datetime_wrapper'];
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {}

  /**
   * Button submit handlers.
   */
  public function redirectToContent(array &$form, FormStateInterface $form_state) {
    $url = Url::fromRoute('unused_data_sweeper.content_list');
    $form_state->setRedirectUrl($url);

  }

  /**
   *
   */
  public function redirectToTaxonomy(array &$form, FormStateInterface $form_state) {
    $url = Url::fromRoute('unused_data_sweeper.taxonomy_list');
    $form_state->setRedirectUrl($url);
  }

  /**
   *
   */
  public function redirectToUser(array &$form, FormStateInterface $form_state) {
    $url = Url::fromRoute('unused_data_sweeper.user_list');
    $form_state->setRedirectUrl($url);
  }

}
