<?php

namespace Drupal\unused_data_sweeper\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Url;
use Drupal\Core\Messenger\MessengerInterface;

/**
 * user filter form.
 */
class UserFilterForm extends FormBase {
  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;
  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;
  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;
  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Constructs a new UserRoleReportController object.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The Drupal service container.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack used to retrieve the current request.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, DateFormatterInterface $date_formatter, RequestStack $request_stack, MessengerInterface $messenger) {
    $this->entityTypeManager = $entity_type_manager;
    $this->dateFormatter = $date_formatter;
    $this->requestStack = $request_stack;
    $this->messenger = $messenger;
  }

  /**
   * Creates an instance of the controller.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The Drupal service container.
   *
   * @return \Drupal\custom_user_role_report\Controller\UserRoleReportController
   *   A UserController instance.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('date.formatter'),
      $container->get('request_stack'),
      $container->get('messenger')

    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'unused_data_sweeper_user_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $request = $this->requestStack->getCurrentRequest();
    $user_name = $request->query->get('user_name');
    $user_status = $request->query->get('user_status');
    $user_role = $request->query->get('user_role');
    $start_date = $request->query->get('start_date');
    $end_date = $request->query->get('end_date');
    $user_list = ['-select-' => '-select-'];
    $user = $this->entityTypeManager->getStorage('user')->loadByProperties();
    foreach ($user as $users) {
      if ($users->getDisplayName() == 'Anonymous') {
        continue;
      }
      $user_list[$users->getDisplayName()] = $users->getDisplayName();
    }
    $form['message'] = [
      '#type' => 'markup',
      '#markup' => $this->t('<b style="font-size: 20px">On this page All the Users listed & Can perform delete and edit operation as well Also filter on the basis of User, User role, Status and Created date</b>'),
    ];
    $form['user_name'] = [
      '#type' => 'select',
      '#title' => 'User',
      '#description' => "Select User Name",
      '#options' => $user_list,
      '#default_value' => !empty($user_name) ? $user_name : '',
      '#attributes' => [
        'class' => ['user-form-control'],
      ],

    ];
    $user_storage = $this->entityTypeManager->getStorage('user');
    $users = $user_storage->loadMultiple();
    $user_roles = ['-select-' => '-select-'];
    foreach ($users as $user) {
      if ($user->isActive()) {
        foreach ($user->getRoles() as $role) {
          if ($role == 'authenticated') {
            continue;
          }
          $user_roles[$role] = $role;
        }
      }
    }

    $form['user_role'] = [
      '#type' => 'select',
      '#title' => $this->t('User Role'),
      '#description' => "Select User Role",
      '#default_value' => !empty($user_role) ? $user_role : ' ',
      '#options' => $user_roles,
      '#attributes' => [
        'class' => ['user-role-field'],
      ],
    ];
    $form['details'] = [
      '#type' => 'details',
      '#title' => t('Search User From Created Date To Choice Date'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#open' => TRUE,
    ];
    $form['details']['start_date'] = [
      '#type' => 'date',
      '#title' => $this->t('Start Date'),
      '#description' => "Select Date",
      '#max' => date('Y-m-d'),
      '#default_value' => !empty($start_date) ? $start_date : ' ',
      '#attributes' => [
        'class' => ['user-start-date-field'],
      ],
    ];

    $form['details']['end_date'] = [
      '#type' => 'date',
      '#title' => $this->t('End Date'),
      '#description' => "Select Date",
      '#max' => date('Y-m-d'),
      '#default_value' => !empty($end_date) ? $end_date : ' ',
      '#attributes' => [
        'class' => ['user-end-date-field'],
      ],
    ];
    $user_status_list = ['-select-' => '-select-', 'Active' => 'Active', 'Blocked' => 'Blocked'];
    $form['user_status'] = [
      '#type' => 'select',
      '#title' => $this->t('User Status'),
      '#description' => 'User Status',
      '#default_value' => !empty($user_status) ? $user_status : '',
      '#options' => $user_status_list,
      '#attributes' => [
        'class' => ['user_status'],
      ],
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
      '#attributes' => [
        'class' => ['btn btn-success'],
      ],
    ];
    $form['actions']['reset'] = [
      '#type' => 'submit',
      '#value' => $this->t('Reset'),
      '#submit' => ['::resetForm'],
      '#attributes' => [
        'class' => ['btn btn-danger'],
      ],
    ];
    $form['actions']['redirect'] = [
      '#type' => 'submit',
      '#value' => $this->t('Go Back'),
      '#submit' => ['::redirectForm'],
      '#attributes' => [
        'class' => ['btn btn-danger'],
      ],
    ];
    $form['#attached']['library'][] = 'unused_data_sweeper/my_styles';
    return $form;
  }

  /**
   *
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    $start_date = $form_state->getValue('start_date');
    $end_date = $form_state->getValue('end_date');
    if (!empty($start_date)) {
      if ($start_date && $end_date && $end_date < $start_date) {
        $form_state->setValue('validate_form', 1);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if ($form_state->getValue('validate_form')) {
      $this->messenger()->addError("Start Date Can't Greater than End Date.");
      $form_state->setRedirect('unused_data_sweeper.user_list');
    }
    else {
      $user_name = $form_state->getValue('user_name');
      $user_role = $form_state->getValue('user_role');
      $start_date = $form_state->getValue('start_date');
      $end_date = $form_state->getValue('end_date');
      if (!empty($start_date) && empty($end_date)) {
        $end_date = date('Y-m-d');
      }
      $user_status = $form_state->getValue('user_status');
      $url = Url::fromRoute('unused_data_sweeper.user_list')->setRouteParameters(['user_name' => $user_name, 'user_role' => $user_role, 'start_date' => $start_date, 'end_date' => $end_date, 'user_status' => $user_status]);
      $form_state->setRedirectUrl($url);
    }
  }

  /**
   *
   */
  public function resetForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRedirect('unused_data_sweeper.user_list');
  }

  /**
   *
   */
  public function redirectForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRedirect('unused_data_sweeper.form');
  }

}
