<?php

namespace Drupal\unused_data_sweeper\Controller;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\Core\Datetime\DrupalDateTime;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Datetime\DateFormatterInterface;

/**
 *
 */
class ContentListController extends ControllerBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;
  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Constructs a new UserRoleReportController object.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The Drupal service container.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack used to retrieve the current request.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, DateFormatterInterface $date_formatter, RequestStack $request_stack) {
    $this->entityTypeManager = $entity_type_manager;
    $this->dateFormatter = $date_formatter;
    $this->requestStack = $request_stack;
  }

  /**
   *
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('date.formatter'),
      $container->get('request_stack'),

    );
  }

  /**
   *
   */
  public function content_list() {
    $data['filter'] = $this->formBuilder()->getForm('Drupal\unused_data_sweeper\Form\content_filter_form');
    $request = $this->requestStack->getCurrentRequest();
    $content_type = $request->query->get('cont_type');

    if (!(empty($request->query->get('start_date'))) && !empty($request->query->get('end_date'))) {
      $start_date = new DrupalDateTime($request->query->get('start_date'));
      $end_date = new DrupalDateTime($request->query->get('end_date'));
      // Convert Date To  Timestamp Formate.
      $start_timestamp = $start_date->getTimestamp();
      $end_timestamp = $end_date->getTimestamp();
      $query = $this->entityTypeManager->getStorage('node')->getQuery()
        ->condition('status', 0)
        ->condition('created', [$start_timestamp, $end_timestamp], 'BETWEEN')
        ->pager(5)
        ->accessCheck(FALSE);
      if (!empty($content_type)) {
        $query->condition('type', $content_type);
      }
    }
    elseif (!empty($content_type) && isset($content_type)) {
      $query = $this->entityTypeManager->getStorage('node')->getQuery()
        ->condition('status', 0)
      // Content type 'article'.
        ->condition('type', $content_type)
        ->pager(5)
        ->accessCheck(FALSE);
    }
    else {
      $query = $this->entityTypeManager->getStorage('node')->getQuery()
        ->condition('status', 0)
        ->pager(5)
        ->accessCheck(FALSE);
    }
    // $pager = $query->extend('Drupal\Core\Database\Query\PagerSelectExtender')->limit(2);
    $nids = $query->execute();
    $nodes = $this->entityTypeManager->getStorage('node')->loadMultiple($nids);

    $header = [
      'title' => $this->t('Title'),
      'type' => $this->t('Content type'),
      'author' => $this->t('Author'),
      'status' => $this->t('Status'),
      'created-date' => $this->t('Created date'),
      'last modified date' => $this->t('Last modified'),
      'Operations' => $this->t('Operations'),
    ];

    $rows = [];
    foreach ($nodes as $node) {
      // Format dates.
      $created_date = $this->dateFormatter->format($node->getCreatedTime(), 'custom', 'd-m-Y H:i:s');
      $changed_date = $this->dateFormatter->format($node->getChangedTime(), 'custom', 'd-m-Y H:i:s');

      $edit_url = Url::fromRoute('entity.node.edit_form', ['node' => $node->id()])->toString();
      $edit_link = new FormattableMarkup('<a href=":link">:label</a>', [':link' => $edit_url, ':label' => $this->t('Edit')]);
      // Create "View" link.
      $view_url = Url::fromRoute('entity.node.canonical', ['node' => $node->id()]);
      $view_link = Link::fromTextAndUrl($this->t('View'), $view_url)->toString();
      // Render the links.
      $links = new FormattableMarkup('@edit_link | @view_link', ['@edit_link' => $edit_link, '@view_link' => $view_link]);

      $rows[] = [
        'title' => $node->getTitle(),
        'type' => $node->gettype(),
        'author' => $node->getOwner()->getDisplayName(),
        'status' => $node->isPublished() ? $this->t('Published') : $this->t('Unpublished'),
        'created-date' => $created_date,
        'last modified date' => $changed_date,
        'edit' => $links,
      ];
    }

    $data['table'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => $this->t('No nodes founds.'),
    ];
    $data['pager'] = [
      '#type' => 'pager',
    ];

    return $data;
  }

}
