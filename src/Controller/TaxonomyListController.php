<?php

namespace Drupal\unused_data_sweeper\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 *
 */
class TaxonomyListController extends ControllerBase {

  /**
   *
   */
  public function unusedTerms() {
    $data_filter_form = $this->formBuilder()->getForm('Drupal\unused_data_sweeper\Form\TaxonomyFilterForm');
    $delete_form = $this->formBuilder()->getForm('Drupal\unused_data_sweeper\Form\TaxonomyDeleteForm');
    return [
      'date_filter_form' => $data_filter_form,
      'delete_form' => $delete_form,
    ];
  }

}
