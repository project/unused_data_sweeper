<?php

namespace Drupal\unused_data_sweeper\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Database\Connection;
use Drupal\Core\Url;
use Drupal\Core\Link;

/**
 * Use Drupal\Core\User;.
 */
class UserListController extends ControllerBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;
  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;
  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * Constructs a new UserRoleReportController object.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The Drupal service container.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack used to retrieve the current request.
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, DateFormatterInterface $date_formatter, RequestStack $request_stack, Connection $connection) {
    $this->entityTypeManager = $entity_type_manager;
    $this->dateFormatter = $date_formatter;
    $this->requestStack = $request_stack;
    $this->connection = $connection;
  }

  /**
   *
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('date.formatter'),
      $container->get('request_stack'),
      $container->get('database')

    );
  }

  /**
   *
   */
  public function user_list() {
    /**
     * Render User Filter Form.
     */
    $data['filter'] = $this->formBuilder()->getForm('Drupal\unused_data_sweeper\Form\UserFilterForm');
    // Fetch Query Data.
    $request = $this->requestStack->getCurrentRequest();
    $user_name = $request->query->get('user_name');
    $user_status = $request->query->get('user_status');
    $user_role = $request->query->get('user_role');
    if (!(empty($request->query->get('start_date'))) && !empty($request->query->get('end_date'))) {
      $start_date = new DrupalDateTime($request->query->get('start_date'));
      $end_date = new DrupalDateTime($request->query->get('end_date'));
      // Convert Date To  Timestamp Formate.
      $start_timestamp = $start_date->getTimestamp();
      $end_timestamp = $end_date->getTimestamp();
    }
    /**
     * Builds the user role report.
     */
    $header = [
      $this->t('Username'),
      $this->t('Roles'),
      $this->t('Creation Date'),
      $this->t('Update Date'),
      $this->t('Status'),
      $this->t('operation'),
    ];
    if ($user_name) {
      $uids_name = $this->entityTypeManager->getStorage('user')->getQuery()
        ->condition('name', $user_name)
      // This line is added.
        ->accessCheck(FALSE)
        ->execute();
    }
    if ($user_role) {
      $uids = $this->connection->select('user__roles', 'ur')
        ->fields('ur', ['entity_id'])
        ->condition('roles_target_id', $user_role)
        ->execute()
        ->fetchCol();
    }

    /**
     * Filter User Between Two Dates
     */
    if (!(empty($start_timestamp)) && !empty($end_timestamp)) {
      // Create an entity query for users.
      $query = $this->entityTypeManager->getStorage('user')->getQuery()
        ->condition('created', [
          $start_timestamp,
          $end_timestamp,
        ], 'BETWEEN')
        ->accessCheck(FALSE);
      $users = $query->execute();

      foreach ($users as $user) {

        $date_user = $this->entityTypeManager()->getStorage('user')->load($user);

        $rows[] = $this->create_table_user($date_user);
      }
    }
    if ((empty($start_timestamp)) && empty($end_timestamp)) {
      /**
       * Filter Based on User Name
       */
      if ($user_role === "-select-") {
        $user_role = 0;
      }
      if ($user_status == "-select-") {
        $user_status = 0;
      }
      if (!(empty($uids_name)) && empty($user_role) && empty($user_status)) {
        foreach ($uids_name as $key => $value) {
          $users = $this->entityTypeManager()->getStorage('user')->load($uids_name[$key]);
          $rows[] = $this->create_table_user($users);
        }
      }
      elseif (!(empty($uids_name)) && empty($user_role) && !empty($user_status)) {
        if ($user_status == 'Active') {
          $user_status = 1;
        }
        else {
          $user_status = 0;
        }
        $query = $this->entityTypeManager->getStorage('user')->getQuery()
          ->condition('uid', $uids_name)
          ->condition('status', $user_status)
        // This line is added.
          ->accessCheck(FALSE)
          ->execute();
        if ($query) {
          foreach ($query as $users) {
            $user = $this->entityTypeManager->getStorage('user')->load($users);
            $rows[] = $this->create_table_user($user);
          }
        }
        else {
          $this->messenger()->addWarning("User Not Found.");
        }
      }
      /**
       * Filter Based on User Role
       */

      elseif ((empty($uids_name)) && !(empty($user_role)) && empty($user_status)) {
        $users = $this->entityTypeManager()->getStorage('user')->loadMultiple($uids);
        foreach ($users as $user) {
          $rows[] = $this->create_table_user($user);
        }
      }
      elseif ((empty($uids_name)) && !(empty($user_role)) && !empty($user_status)) {
        if ($user_status == 'Active') {
          $user_status = 1;
        }
        else {
          $user_status = 0;
        }
        $users = $this->entityTypeManager()->getStorage('user')->loadMultiple($uids);
        foreach ($users as $user) {
          $query = $this->entityTypeManager->getStorage('user')->getQuery()
            ->condition('uid', $user->id())
            ->condition('status', $user_status)
          // This line is added.
            ->accessCheck(FALSE)
            ->execute();
          if ($query) {
            foreach ($query as $users) {
              $user = $this->entityTypeManager->getStorage('user')->load($users);
              $rows[] = $this->create_table_user($user);
            }
          }
        }
      }
      /**
       * Filter Based on User Name && User Role
       */

      elseif (!(empty($user_name)) && !(empty($user_role)) && empty($user_status)) {
        $users = $this->entityTypeManager()->getStorage('user')->loadMultiple($uids);
        foreach ($users as $user) {
          foreach ($uids_name as $key => $value) {
            if ($user->id() == $uids_name[$key]) {
              $rows[] = $this->create_table_user($user);
            }
          }
        }
      }
      elseif (!(empty($uids_name)) && !(empty($user_role)) && !empty($user_status)) {
        if ($user_status == 'Active') {
          $user_status = 1;
        }
        else {
          $user_status = 0;
        }
        $users = $this->entityTypeManager()->getStorage('user')->loadMultiple($uids);
        foreach ($users as $user) {
          foreach ($uids_name as $key => $value) {
            if ($user->id() == $uids_name[$key]) {
              $query = $this->entityTypeManager->getStorage('user')->getQuery()
                ->condition('uid', $user->id())
                ->condition('status', $user_status)
              // This line is added.
                ->accessCheck(FALSE)
                ->execute();
              if ($query) {
                $rows[] = $this->create_table_user($user);
              }
            }
          }
        }
      }
      elseif ($user_status == 'Active') {

        // Based On User Status.
        $uid_status = $this->entityTypeManager->getStorage('user')->getQuery()
        // 1 represents 'Active' status.
          ->condition('status', 1)
        // This line is added.
          ->accessCheck(FALSE)
          ->execute();
        $users = $this->entityTypeManager()->getStorage('user')->loadMultiple($uid_status);
        foreach ($users as $user) {
          if ($user->isActive()) {
            $rows[] = $this->create_table_user($user);
          }
        }
      }
      elseif ($user_status == 'Blocked') {
        // Based On User Status.
        $uid_status = $this->entityTypeManager->getStorage('user')->getQuery()
        // 1 represents 'Active' status.
          ->condition('status', 0)
        // This line is added.
          ->accessCheck(FALSE)
          ->execute();
        $users = $this->entityTypeManager()->getStorage('user')->loadMultiple($uid_status);
        foreach ($users as $user) {
          if ($user->isBlocked()) {
            $rows[] = $this->create_table_user($user);
          }
        }
      }
      else {
        $users = $this->entityTypeManager()->getStorage('user')->loadMultiple();
        foreach ($users as $user) {
          $rows[] = $this->create_table_user($user);
        }

      }
    }
    // Build the table render array.
    $data['table'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => !empty($rows) ? $rows : '',
      '#empty' => $this->t('Users Not Found.'),
    ];
    $data['pager'] = [
      '#type' => 'pager',
    ];
    // Prevent caching.
    $data['#cache']['max-age'] = 0;
    return $data;
  }

  /**
   *
   */
  protected function create_table_user($user) {
    $user_roles = [];
    $edit_url = Url::fromRoute('entity.user.edit_form', ['user' => $user->id()]);
    $edit_link = Link::fromTextAndUrl(t('Edit'), $edit_url)->toString();
    // Generate the delete link.
    $delete_url = Url::fromRoute('entity.user.cancel_form', ['user' => $user->id()]);
    $delete_url->setOption('query', ['destination' => '/admin/people']);
    $delete_url->setRouteParameter('user', $user->id());
    $delete_link = Link::fromTextAndUrl(t('Delete'), $delete_url)->toString();
    // Render the links.
    $links = new FormattableMarkup('@edit_link | @delete_link', ['@edit_link' => $edit_link, '@delete_link' => $delete_link]);
    foreach ($user->getRoles() as $role) {
      $user_roles[] = $role;
    }
    return $rows[] = [
      'data' => [
        'username' => $user->getDisplayName(),
        'roles' => implode(', ', $user_roles),
        'creation_date' => $this->dateFormatter->format($user->getCreatedTime(), 'custom', 'd-m-Y H:i:s'),
        'update_date' => $this->dateFormatter->format($user->getChangedTime(), 'short'),
        'status' => $user->isBlocked() ? $this->t('Blocked') : $this->t('Active'),
        'operation' => $links,
      ],
    ];
  }

}
